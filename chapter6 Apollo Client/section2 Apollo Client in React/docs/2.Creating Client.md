# Creating and Connecting Client to Graphql Endpoint

Great, now that you have all the dependencies you need, let’s create your Apollo Client. The only thing you need to get started is the endpoint for your GraphQL server. If you don’t pass in uri directly, it defaults to the /graphql endpoint on the same host your app is served from.

For this tutorial we have built special graphql enpoint for `BookStore App` with data inside it. You can go to playground here https://mern-course-server.herokuapp.com/graphql

### Housekeeping 
In order to connect the whole react app to graphql endpoint with Apollo Client, the client has to be created in the starting point of React App which is index.js or App.js in src folder.<br>

### Creating Client

So, now go to index.js in your src folder and insert the following code snippet to import necessary Apollo Client module: 
```js
import ApolloClient from "apollo-boost";
```
Then we can create Client of our App with our special BookStore graphql endpoint: 
```js
const client = new ApolloClient({
  uri: "https://mern-course-server.herokuapp.com/graphql"
});
```

### Connecting Client

To connect Apollo Client to React, you will need to use the ApolloProvider component exported from react-apollo. The ApolloProvider is similar to React’s context provider. It wraps your React app and places the client on the context, which allows you to access it from anywhere in your component tree.

First, import ApolloProvider in index.js: 
```js
import { ApolloProvider } from "react-apollo";
```
In our index.js typically create-react-app brings App from App.js. In order to place our ApolloProvider highest order, we should wrap our App around with ApolloProvider, just like this: 
```javascript
<ApolloProvider client={client}>
    <App />
</ApolloProvider>
```

ApolloPorvider, as you can see from the code above takes client we created above as a prop.

Now, our React App is ready to fetch data from remote Graphql endpoint. In the next article we explore how to fetch data from endpoint using `<Query>` and `<Mutation>` component.
