# Introduction

### What is Apollo Client and what does it do?

Apollo Client is the best way to use GraphQL to build client applications. The client is designed to help you quickly build a UI that fetches data with GraphQL, and can be used with any JavaScript front-end. The client is:

* **Incrementally adoptable**: You can drop it into an existing app today.
* **Universally compatible**: Apollo works with any build setup, any GraphQL server, and any GraphQL schema.
* **Simple to get started with**: Start loading data right away and learn about advanced features later.
* **Inspectable and understandable**: Interrogate and understand exactly what is happening in an application.
* **Built for interactive apps**: Application users make changes and see them reflected immediately.
* **Small and flexible**: You don’t get stuff your application doesn’t need.
* **Community driven**: Apollo is driven by the community and serves a variety of use cases.

### HouseKeeping
In order explain the theories in a real life example we have to start by making a new react js app. 


In the section project folder you can find new ReactJS Starter.
We will use this project to teach you about Apollo Client Integration.


From next section we will explore Apollo Client Integration in React

>If you want to know more about performance of Apollo Client, [look at here](https://www.apollographql.com/docs/react/why-apollo.html#case-studies)
