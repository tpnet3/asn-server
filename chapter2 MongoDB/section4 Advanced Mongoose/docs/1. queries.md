# CRUD(create, read, update, delete) operations on Database with mongoose queries
We shall discover usage of following queries in this article: 
* **save()**, save() is not query, but essentially we can use it to create new document as we have seen in the previous section
* **findOne()**
* **findOneAndUpdate()**
* **findOneAndDelete()**

The amount of queries in mongoose is relatively big, you can check out official documentation of queries [here](https://mongoosejs.com/docs/queries.html)<br>

### Creating a Document

We will put only code snippet of creating document inside setup/index.js file here with different details becuase we have dicussed creating new document in previous section.
```javascript
const Book = require('./../models/books');
require('./../index.js');
const result = Book({
    name: "Pro React 1st ed. Edition",
    author: "Cassio de Sousa Antonio",
    pics: [
      "https://images-na.ssl-images-amazon.com/images/I/31SXwhHaDsL._SX348_BO1,204,203,200_.jpg"
    ],
    price: 32.99,
    short_desc: "some information",
    long_desc: "some information",
    date_added: Date.now()
}).save();
if(!result){
    console.log("Insert Unsuccessful");
}
else{
    console.log(result);
}
```

Before touching on the other queries, let's tide up the workspace: 
* Go to index.js inside setup folder
* Leave only following lines, and delete all other lines: 

```javascript
const Book = require('./../models/books');
require('./../index.js');

```

### Reading a document from a database

To read a document we will use findOne() query method of mongoose, certainly if you look up to the official documentation of mongoose queries, there are many other queries to read a document from database. 

* In index.js file insert following lines of code:

```js
const readQuery = async (price)=>{
    const result = await Book.findOne({price: price}).exec();
    if(!result){
        return null
    }
    else{
        return result
    }
}
const book = await readQuery(32.99);
if(!book){
    console.log(`No Book Found priced at 32.99$`);
}
else{
    console.log(book);
}
```
In the code snippet above, Book mongoose model's findOne() query method is called with a certain condition, price to be equal to 32.99 in the case above. We made asyncronous arrow function called readQuery and since findOne returns Promise, we are resloving Primise returned by the findOne() method by putting await in front of readQuery() finction call.
If findOne can not find document with given condition, it will return false and readQuery function returns null respectively.


### Updating a document in database

Updating the document is also so simple and similar to the read document query:

```js
const updateQuery = async (price, new_price)=>{
    const result = await Book.findOneAndUpdate({price: price}, {$set: {price: new_price}}).exec();
    if(!result){
        return null
    }
    else{
        return result
    }
}
const updated = await updateQuery(32.99, 30.00);
if(!updated){
    console.log(`No Book Found priced at 32.99$`);
}
else{
    console.log(updated);
}
```
Code snippet above has only difference with new `{$set: {price: new_price}}` section from findOne() query. This section is the critical section of the update query in mongoose. If document is found with given condition(s) it will update the filed inside `$set` object, price with new_price in the given code snippet.

### Deleting a document from database

Deleting a document from database is also similar to queries above described:

```js
const deleteQuery = async (price)=>{
    const result = await Book.findOneAndDelete({price: price}).exec();
    if(!result){
        return null
    }
    else{
        return result
    }
}
const deleted_book = await deleteQuery(30.00);
if(!deleted_book){
    console.log(`No Book Found priced at 32.99$`);
}
else{
    console.log(deleted_book);
}
```
If the findOneAndDelete() can find a book with given condition it will delete that document from database and return that deleted document as promise. 


### Article Challenge
*Try To Add(insert) new document, read it, update it and delete it*

We recommend you to play with all of mongoose queries in official documentation in order to improve your understanding.
In the next article we will learn about instance methods of Mongoose. 