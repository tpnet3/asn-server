const Book = require('./../models/books');

require('./../index.js');

const result = Book({
    name: "You Don't Know JS: Up & Going 1st Edition",
    author: "Kyle Simpson",
    pics: [
      "https://images-na.ssl-images-amazon.com/images/I/41FhogvNebL._SX331_BO1,204,203,200_.jpg",
      "https://scontent-iad3-1.cdninstagram.com/vp/6e9678a1817b7221a24f4e62fa86aab8/5CF1788A/t51.2885-15/e35/46014673_261980321338988_7254308247593613649_n.jpg?_nc_ht=scontent-iad3-1.cdninstagram.com"
    ],
    price: 12.09,
    short_desc: "some information",
    long_desc: "some information",
    date_added: Date.now()
}).save();
if(!result){
    console.log("Insert Unsuccessful");
}
else{
    console.log(result);
}