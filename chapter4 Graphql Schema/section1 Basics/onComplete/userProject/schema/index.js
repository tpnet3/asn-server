const { gql } = require('apollo-server');
const types = require('./types');

const query = gql`
  type Query {
      books: [Book]
    }
  `
  module.exports = [query, types];