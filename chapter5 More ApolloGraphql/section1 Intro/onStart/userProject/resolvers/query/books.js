const Book = require('./../../db/models/books.js');

const books = async ()=>{
  const result = await Book.getBooks();
  if(result===null){
  return null;
  }
  return result;
}

const getSingleBook = async (obj, {price})=>{
  const result = await await Book.getSingleBook(price);
  if(result===null){
  return null;
  }
  return result;
}
module.exports = {
  books,//already exported before
  getSingleBook //newly created method
}