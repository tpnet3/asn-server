const { gql } = require('apollo-server');
const types = require('./types');
const query = gql`
  type Query {
      books: [Book],
      getSingleBook(
          price: Float
      ): Book
    }
  `
  module.exports = [query, types];