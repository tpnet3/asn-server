# Implementing Mutation in Graphql

So far in the examples mostly we have covered working with queries in Graphql Apollo. That's because the structure of Query and Mutation are exactly same. The only difference will happen when you implement the logical part of Mutation and when you work with Apollo Client. In this article we will show you how to implement Mutation in Graphql. 

### Logic
Think about our BookStore app. What kind of mutation might it need? It would probably need "like a book". In that case we can call this action as "like mutation" because it will create a new document inside our Liked Model. <br>
Whaaaat, What is "Liked Model"😱😱😱. <br>
Liked Model is model in the database where we will put books that are liked by some user of our database. 
As we have discussed before Liked model does not need a lot of data, and it is logical to reduce the redundancy of data. Liked Model only needs: 
* user_id: String 
* book_id: String
* date_liked: Number  //It is always good practice to track the data of a certain action<br>
We are sure you can make this model yourself, therefore first take 10 minutes and make this model now and continue with the article since leftover of this article will assume that you have made the Liked Model in the right place. 😇😇😇<br><br>
⌛ A few moments later....<br><br>
Good job, let's continue with the article🔍🔍🔍<br>
Most of the actual work of mutation happens when we create, update or delete data in database, therefore we should start implementing any mutation or query by making static method in mongoose model file. <br>
Let's make the logic of likeBook mutation: 
* Bring _id of book, and _id of user
* Put them into database as a document of Liked Model
* Return true if actions above were successful
* Return false if the above actions were not successful
  
Now it is time to implement these actions as a static method of Liked Model: 

* Go to liked.js inside db/models directory and add this code: 
```js
likedSchema.statics.likeBook = async function (book_id, user_id) {
  var inliked = await this.inLiked(book_id, user_id);
  if(inliked!=null){
    throw new Error("alreadyInLiked");
  }
  var result = this.create({
    user_id: user_id,
    book_id: book_id,
    date_liked: Date.now()
  })
  if(!result){
    return false;
  }
  else{
    return true;
  }
}
likedSchema.statics.inLiked = async function (book_id, user_id) {
  return this.findOne({$and: [{book_id: book_id}, {user_id: user_id}]}); //fetching products in cart according to book_id and user_id
}
``` 
If you noticed from the code snipped, we are getting two arguments which are book_id, user_id. It is always recommended to check if the data with given book_id and user_id already exists or not, therefore we have checked that in inLiked function.
After that, we have inserted new document with Model.create() method. <br><br>

Now, we can implement this likeBook functionality in our resolvers and schema respectively:

* Go to mutation folder inside resolvers and create like.js file where we will define likeBook resolver:
    * Inside like.js file we shall add the following code snippet: 
    ```js
    const Like = require('./../../db/models/liked.js');  //here we are assuming that the file where you have defined Liked Model to be "liked.js". If this is not the case make sure you change the name of directory in accordance with your naming convention
    const likeBook = async (obj, {book_id, user_id})=>{
    const result = await Like.likeBook(book_id, user_id);
    if(!result){
      throw new Error(`addToLikedNull`)
    }
    return result; 
    }

    module.export = {
      likeBook
    }
    ```
* Following that, lets point new likeBook in resolvers entry point file, namely index.js resolvers folder. Change the whole code into this: 
```js
const books = require('./query/books')
const liked = require('./mutation/like')
module.exports = {
    Query: {
        books: books.books,
        getSingleBook: books.getSingleBook
    },
    Mutation: {
        likeBook: liked.likeBook
    }
}
```
If you have noticed we have created one more object called Mutation where we point our mutation resolvers in the above code snippet.

* Last, we should add new mutation definition to index.js inside schema folder: 
```js 
const { gql } = require('apollo-server');
const types = require('./types');
const query = gql`
  type Query {
      books: [Book],
      getSingleBook(
          price: Float
      ): Book
    }
  type Mutation {
      likeBook(
          book_id: String!,
          user_id: String!
      ): Boolean  
    }
  `
  module.exports = [query, types];
```
Defining a mutation with argument is finished, now you can check this mutation in Playground.<br>
To mutate with argument you can write this in Playground: 
```graphql
mutation likeBook{
    likeBook(book_id: "book_id", user_id: "user_id")
}
```
>Notice that, even though we have book model and actual book in our database, we still do not have user model and actual user with _id. But when you implement user model and can get actual user _id we recommend to test this mutation with those actual data. 

### Article Challenge
*Try firing likeBook mutation in Playground with the same book_id and user_id value second time, notice the returning result of the second mutation with the same values*

>The result should be error with *alreadyInLiked* message because we have checked in the likeBook static function that if a particular book with a particular user_id exists or not.

In the next article we will learn about Graphql Pagination