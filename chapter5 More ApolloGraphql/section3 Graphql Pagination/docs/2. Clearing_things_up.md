# Clearing Things Up

Before we move on to actual development of methods and resolvers in the next article, let's explore some vocabulary and schema of paginated query we will be using, because it is critical to understand them before we continue:
* First, We shall make paginated query of books inside index.js in schema directory:
```grapqhl
books(
    pageSize: Int,
    next: Int,
):paginatedBooks
```
* Next, if you noticed from above qeuery of books it is returning custom type called **paginatedBooks**, that means we have to make this type inside types.js inside schema directory:

```graphql
type paginatedBooks {
    hasMore: Boolean!,
    next: Int!,
    books: [Book]
}
```
I think you know what **[Book]** mean. For reference, **books: [Book]** means paginatedBooks has field which actually holds array of Books we present to user. 

* Now let's come to the vocabularies that might not be clear to you: 
    * **pageSize**: Number of documents we want to fetch at one time
    * **next** in query: The starting point(index) of documents array to be fetched. If this argument in query has the value of 1 this means the starting element to be fetched is the first element of the documents array.
    * **next** in type: index of the document in the database just after the last element fetched in the previous fetch, if this field coming from API has the value of 10 this means next fetch should start from element 10 in the domuments array
    * **hasMore**: Boolean value indicating the existence or absence of more documents which can be fetched in the next fetch. If this field has the value of false this means we do not have to call the next fetch. 

### Article Challenge

*What do you think what will be the the value of **next** in query when we query books for the first time?*

>If we apply pagination to our pages in our app, in the first call to our paginated wuery **next** value vould mostly be 1. 

In the next article we shall learn implementing books method as static method of Book model and implementing this method in query resolvers. 