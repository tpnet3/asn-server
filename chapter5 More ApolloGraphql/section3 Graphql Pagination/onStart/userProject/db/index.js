const mongoose = require('mongoose');

var dbURI = "mongodb://test:test123@ds239055.mlab.com:39055/mern-learning";  //for online db

mongoose.connect(dbURI, { useNewUrlParser: true })

mongoose.connection.on('connected', ()=>{
  console.log("Connected to Database");
});

mongoose.connection.on('error', (err)=>{
  throw new Error("Failed to Connect to Database "+err);
});

mongoose.connection.on('disconnected', (err)=>{
  throw new Error("Database Disconnected "+err);
});
