const Book = require('./../models/books');
require('./../index.js');

const readQuery = async (price)=>{
    const result = await Book.findOne({price: price}).exec();
    if(!result){
        return null
    }
    else{
        return result
    }
}
const book = await readQuery(32.99);
if(!book){
    console.log(`No Book Found priced at 32.99$`);
}
else{
    console.log(book);
}

const updateQuery = async (price, new_price)=>{
    const result = await Book.findOneAndUpdate({price: price}, {$set: {price: new_price}}).exec();
    if(!result){
        return null
    }
    else{
        return result
    }
}
const updated = await updateQuery(32.99, 30.00);
if(!updated){
    console.log(`No Book Found priced at 32.99$`);
}
else{
    console.log(updated);
}

const deleteQuery = async (price)=>{
    const result = await Book.findOneAndDelete({price: price}).exec();
    if(!result){
        return null
    }
    else{
        return result
    }
}
const deleted_book = await deleteQuery(30.00);
if(!deleted_book){
    console.log(`No Book Found priced at 32.99$`);
}
else{
    console.log(deleted_book);
}