const User = require('../../db/models/user')

const signUpWithPassword = async (obj, {username, password}, ctx)=>{
    const token = await User.SignUpWithPassword(username, password);
    ctx.headers.accessToken = token;
    return token;
  }

  module.exports={
      signUpWithPassword
  }
  