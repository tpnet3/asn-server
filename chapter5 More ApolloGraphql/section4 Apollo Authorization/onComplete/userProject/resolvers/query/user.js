const User = require('../../db/models/user')

const signInWithPassword = async (obj, {username, password}, ctx)=>{
    const token = await User.signInWithPassword(username, password);
    ctx.headers.accessToken = token;
    if(token){
      ctx.headers.accessToken = token;
    }
    return token;
  }
  
module.exports={
    signInWithPassword
}
  