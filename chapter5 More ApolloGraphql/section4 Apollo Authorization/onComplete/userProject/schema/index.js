const { gql } = require('apollo-server');
const types = require('./types');
const schema = gql`
  type Query {
    books(
        pageSize: Int,
        next: Int,
      ):paginatedBooks,
      getSingleBook(
          price: Float
      ): Book
      signInWithPassword(
      username: String!,
      password: String!
    ): String!
    },
  type Mutation {
    signUpWithPassword(
      username: String!,
      password: String!,
    ):String!,
    likeBook(
      book_id: String!
    ):Boolean
    }
  `
  module.exports = [schema, types];