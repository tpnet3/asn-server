const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const secret = process.env.SECRET || 'secret';

const userSchema = new mongoose.Schema({
    username: String,
    password: String
  })

  userSchema.statics.SignUpWithPassword = async function (username, password) {
    const userName = await this.findOne({username: username}).exec();
    
    if(userName){
      throw new Error(`usernameTaken`);
    }
    
    const user = await this.create({
      username: username,
      password: password,
    });
    
    if(!user){
      throw new Error("signUpFailed");
    }
    const expiresIn = 7776000;
    
    const accessToken = jwt.sign({
      _id: user._id,
    }, secret, {expiresIn: expiresIn}); //made token
    
    return accessToken;
  }
  
  userSchema.statics.signInWithPassword = async function (username, password) { 
    const user = await this.findOne({username: username}).exec();
    if(!user){
      throw new Error("userNotFound");
    }
    if(user.password === password){
      const expiresIn = 7776000;
      const accessToken = jwt.sign({
        _id: user._id,
      }, secret, {expiresIn: expiresIn}); //made token
      return accessToken;
    }
    else{
      throw new Error("passwordIncorrect");
    }
  }

  userSchema.statics.verifyUser = async function (token) {   
    var decoded_object = await jwt.verify(token, secret);
    if(!decoded_object){
      return false;
    }
    var user = await this.findById({_id: decoded_object._id}).exec();
    if(!user){
      return false
    }
    return user;
  }

  module.exports = mongoose.model('User', userSchema);