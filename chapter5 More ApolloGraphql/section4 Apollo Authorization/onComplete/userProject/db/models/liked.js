const mongoose = require('mongoose');
// define a schema
const likedSchema = new mongoose.Schema({
    user_id: String,
    book_id: String,
    date_liked: Number
})
likedSchema.statics.likeBook = async function (book_id, ctx) {
  if(ctx.headers.accessToken==null){
    throw new Error(`tokenFailed`);
  }
  const user = await this.model('User').verifyUser(ctx.headers.accessToken);
  if(!user){
    throw new Error(`tokenFailed`)
  }
  var inliked = await this.inLiked(book_id, user._id);
  if(inliked!=null){
    throw new Error("alreadyInLiked");
  }
  
  var result = this.create({
    user_id: user._id,
    book_id: book_id,
    date_liked: Date.now()
  })

  if(!result){
    return false;
  }
  else{
    return true;
  }
}
  likedSchema.statics.inLiked = async function (book_id, user_id) {
    return this.findOne({$and: [{book_id: book_id}, {user_id: user_id}]}); //fetching products in cart according to book_id and user_id
  }
module.exports = mongoose.model('Liked', likedSchema);
