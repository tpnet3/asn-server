# Introduction
### What is Apollo Server and what does it do?

Apollo Server is the best way to quickly build a production-ready, self-documenting API for GraphQL clients, using data from any source.

It’s open-source and works great as a stand-alone server, an addon to an existing Node.js HTTP server, or in “serverless” environments.

![apollo](https://www.apollographql.com/docs/apollo-server/images/index-diagram.svg)

Apollo Server implements a spec-compliant GraphQL server which can be queried from any GraphQL client, including Apollo Client, enabling:

1. **An easy start**, so front-end and back-end developers can start fetching data quickly.
2. **Incremental adoption**, allowing advanced features to be added when they’re needed.
3. **Universal compatibility** with any data source, any build tool and any GraphQL client.
4. **Production readiness**, and what you build in development works great in production.

In the following articles we will learn how to set up Apollo Server
