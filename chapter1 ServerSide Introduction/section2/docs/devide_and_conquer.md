# Better approach to better and faster development.

It is great to see you reach this point, since "starting the development is the halfway through the finish", most of the experienced developers say.<br> <br>
Have you heard of "Divide and Conquer" approach? The smarter way of organizing the develpment cycle.<br><br>
Since, We have to deal with **MongoDB**, **Graphql**, **Apollo Server** in order to learn to build the ready API we have chosen dividing the whole tutorial into 3 sub-tutorials.<br><br>

Three sub-tutorials are as follows: 

1. **MongoDB with Mongoose Implementation**: Learning and implementing MongoDB first makes our job more efficient. Because, while building our Apollo Server and Graphql schema later, we should be able to understand MoongoDB and connect to a real remote, or local Mongo Database where our real data will be stored.  
2. **Apollo Server Implementation**: The reason why you are going to learn Apollo Server before building the Graphql schema is because of better develpoer experience. Developing your graphql schema and resolvers without testing them is not a good way, because you have to debug and make it better through the lens of GraphQL Playground which is avalable only after you set up your apollo server.  Once you get your apollo server work properly, you are good to go to make your schema and test your schema. While you are developing your own project server API in the future, we recommend you to follow this cycle, since it has proved itself to be faster, and consistent. 
3. **Graphql Schema Implementation**: As described above, implementing graphql at the end grants us much faster development, since once we get our real Database ready, we only work with real dynamic data which means we should not change to dynamic data at the end of development if we would use static data instead, and with apollo in place we can test our graphql resolvers and debug them. 
<br>

### Article Challenge
*Why do you think learning and implementing Mongo DB before we deal with Apollo and Graphql is useful?*

I am sure you have got it right if you have read the tutorial above!
Let's go to next chapter ⏭️⏭️⏭️
